/*
 * Algebra labs.
 */
 
package com.example.demo.config;

import javax.inject.Inject;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.example.demo.persistence.ItemRepository;
import com.example.demo.service.Catalog;
import com.example.demo.service.CatalogImpl;

@Configuration
public class SpringServicesConfig {
	
	// TODO: Inject the repository
	@Inject
	ItemRepository repository;
	
	// TODO: Define the catalog bean
	@Bean
	public Catalog catalog() {
		CatalogImpl catalog = new CatalogImpl(repository);
		return catalog;
	}

}