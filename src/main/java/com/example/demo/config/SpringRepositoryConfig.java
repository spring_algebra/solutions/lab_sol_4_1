/*
 * Algebra labs.
 */
 
package com.example.demo.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.example.demo.persistence.ItemRepository;
import com.example.demo.persistence.JpaItemRepository;

@Configuration
public class SpringRepositoryConfig {
	
	// Define itemRepository bean
	@Bean
	public ItemRepository itemRepository() {
		return new JpaItemRepository();
	}

}