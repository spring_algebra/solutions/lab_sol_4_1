/*
 * Algebra labs.
 */

package com.example.demo.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.ImportResource;

@Configuration
@Import({SpringRepositoryConfig.class, SpringServicesConfig.class})
@ImportResource("classpath:configuration/applicationContext.xml")
public class SpringConfig {
	    	
}
