/*
 * Algebra labs.
 */

package com.example.demo.domain;

public enum MusicCategory {
	Blues,Classical,Jazz,Rap,Country, Pop, Alternative, Rock, Classic_Rock
}
